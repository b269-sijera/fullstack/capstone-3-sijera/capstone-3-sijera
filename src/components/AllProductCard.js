import {useState, useEffect} from 'react';

import {Link} from "react-router-dom";

// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function AllProductCard({product}) {

    // Deconstruct the course properties into their own variables
    const { name, description, price, _id } = product;



return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        {/*<Card.Subtitle>Count: {count} </Card.Subtitle>
                        <Card.Subtitle>Seats: {seats}</Card.Subtitle>*/}
                        {/*<Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>*/}
                        <Button className="mx-1 text-white" variant="info" as={Link} to={`/update/${_id}`}>Update Details</Button>
                        <Button className="mx-2" variant="primary" as={Link} to={`/archive/${_id}`}>Update Status</Button>
                        <Button className="mx-1" variant="secondary" as={Link} to={`/admin`}>Go Back</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}


