import {useState, useContext} from 'react';

import {Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar className="navbar" expand="lg">
    <Container>
      <Navbar.Brand className="navbar-brand" as={Link} to="/">
        BARE SKIN
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link className="nav-item mx-2" as={NavLink} to="/">
            Home
          </Nav.Link>
          <Nav.Link className="nav-item mx-2" as={NavLink} to="/products">
            Products
          </Nav.Link>
          {user.isAdmin && (
            <Nav.Link className="nav-item mx-2" as={NavLink} to="/admin">
              Admin
            </Nav.Link>
          )}
          {user.id ? (
            <Nav.Link className="nav-item mx-2" as={NavLink} to="/logout">
              Logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link className="nav-item mx-2" as={NavLink} to="/login">
                Login
              </Nav.Link>
              <Nav.Link className="nav-item mx-2" as={NavLink} to="/register">
                Register
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

{/*<nav class="navbar navbar-expand-md navbar-light position-fixed fixed-top">
  <a class="navbar-brand ml-5" href="#">LENKELT</a>
  <button class="navbar-toggler bg-secondary" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mx-auto">
      <li class="nav-item active">
        <a class="nav-link mx-2" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link mx-2" href="#about-me-head">About Me</a>
      </li>
      <li class="nav-item">
        <a class="nav-link mx-2" href="#projects-head">Projects</a>
      </li>       
    </ul>
    <form class="form-inline my-2 my-lg-0 mr-5">
      <button id="contact-button" class="btn my-2 my-sm-0" type="submit"><a id="contact-text" class="text-decoration-none" href="#contact-head">Contact</a></button>
    </form>
  </div>
</nav>*/}