import {Link, NavLink} from 'react-router-dom';


import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';


export default function Banner() {
  return (
    <Card className="banner text-center mt-3">
      <Card.Body>
        <Card.Title id="banner-title">BARE SKIN</Card.Title>
        <Card.Text>
         "Nature's beauty is the best skincare recipe."
        </Card.Text>
        <Button id="banner-bttn" className="rounded-pill px-5 py-2" variant="success" as={Link} to={`/products`}>Explore</Button>
      </Card.Body>
    </Card>
  );
}
