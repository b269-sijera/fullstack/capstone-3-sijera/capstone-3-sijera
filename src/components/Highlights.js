import Button from 'react-bootstrap/Button';
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
  return (
    <Row className="mt-3 mb-3">
    <Col xs={12} md={4}>
    <Card className="cardHighlight p-3 mx-4" style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://worldaerosols.com/wp-content/uploads/2021/09/Sustainable-cosmetics-768x513.jpg" />
      <Card.Body >
        <Card.Title id="highlight-title">Natural Ingredients</Card.Title>
        <Card.Text>
          Using all-natural ingredient skincare products can provide numerous benefits. They tend to be gentler on the skin, as they are free of harsh chemicals and synthetic fragrances. Natural ingredients can also nourish the skin and promote a healthy, youthful appearance. Additionally, they are eco-friendly and cruelty-free.
        </Card.Text>
      </Card.Body>
    </Card>
    </Col>

    <Col xs={12} md={4}>
    <Card className="cardHighlight p-3 mx-5" style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://img.freepik.com/premium-photo/liquid-green-balm-texture-smudge-concept-natural-cosmetics-homemade-beeswax-sample-aloe-vera-cosmetic-gel-texture-background-skincare-moisturizing-product_284704-1043.jpg"  />
      <Card.Body className="cardBody">
        <Card.Title id="highlight-title">For All Skin Types</Card.Title>
        <Card.Text>
          BARESKIN products are suitable for all skin types, including sensitive, oily, dry, and combination skin. Their formulations are carefully crafted to balance and nourish the skin, without causing irritation or clogging pores. Whether you have normal, problematic, or mature skin, there is a product that can meet your needs.
        </Card.Text>
      </Card.Body>
    </Card>
    </Col>

    <Col xs={12} md={4}>
    <Card className="cardHighlight p-3 mx-5" style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://media.istockphoto.com/id/821663580/photo/green-cosmetic-arrangement.jpg?s=612x612&w=0&k=20&c=t9ZS3_XtZ8F1La8AKuGASWLTt44LIPi0TJAMXNlG-H4="  />
      <Card.Body>
        <Card.Title id="highlight-title">Eco-Friendly Packaging</Card.Title>
        <Card.Text className="text-justify">
          BARESKIN prioritizes sustainability by using eco-friendly packaging in our products. We aim to minimize waste and reduce our environmental footprint by using recyclable and biodegradable materials whenever possible. By choosing BARESKIN products, customers can enjoy healthy, glowing skin while also supporting eco-conscious practices.
        </Card.Text>
      </Card.Body>
    </Card>
    </Col>
    </Row>
  );
}

// import Card from 'react-bootstrap/Card';

// export default function Banner() {
//   return (
//     <Card className="bg-dark text-white w-50 mx-auto">
//       <Card.Img className="w-100  d-block" src="https://media.istockphoto.com/id/1320934166/photo/cosmetic-skin-care-products-on-green-leaves.jpg?s=612x612&w=0&k=20&c=X4pwnTaBzXHDOGZlcdJdlKxmYd__61xboHVIiR5JMIk=" />
//       <Card.ImgOverlay>
//         <Card.Title>BARE SKIN</Card.Title>
//         <Card.Text>
//           Introducing our eco-friendly skincare brand, committed to providing effective and sustainable solutions for your skin. Our products are made with natural ingredients and packaged in biodegradable or recyclable materials, reducing our impact on the environment. We believe in ethical practices, transparency, and empowering you to make eco-conscious choices for your beauty routine.
//         </Card.Text>
//         <Card.Text>Last updated 3 mins ago</Card.Text>
//       </Card.ImgOverlay>
//     </Card>
//   );
// }









/*import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
    return (
        <Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn from Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Study Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Be Part of Our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}
*/