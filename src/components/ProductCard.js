import {useState, useEffect} from 'react';

import {Link} from "react-router-dom";

import { Button, Row, Col, Card } from 'react-bootstrap';








export default function ProductCard({product}) {
    const { name, description, price, _id } = product;

  return (
    <Row className="mt-3 mb-3">
    <Col xs={12} md={4}>
    <Card className="productCard p-3 mx-4" style={{ width: '18rem' }}>
      <Card.Body >
        <Card.Title><h4 className="product-name">{name}</h4></Card.Title>
        <Card.Subtitle className="description">Description</Card.Subtitle>
        <Card.Text className="product-description">{description}</Card.Text>
        <Card.Subtitle className="price">Price</Card.Subtitle>
        <Card.Text className="product-price">PHP {price}</Card.Text>
        <Button className="product-button" as={Link} to={`/products/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
    </Col>
    </Row>
  );
}

{/*<Row lg={3}>
  {productList &&
    productList.map((product) => {
      const { id, title, price, category, description, image } =
        product;
      return (
        <Col className="d-flex">
          <Card className="flex-fill" key={id} className="productlist">
            <Card.Img variant="top" src={"#"} />
            <Card.Body>
              <Card.Title>{title}</Card.Title>
              <Card.Text>{description}</Card.Text>
              <Card.Text>{category}</Card.Text>
              <Card.Text>{price}</Card.Text>
              <Button variant="primary">Add to cart</Button>
            </Card.Body>
          </Card>
        </Col>
      );
    })}
</Row>*/}




// export default function ProductCard({product}) {

//     // Deconstruct the course properties into their own variables
//     const { name, description, price, _id } = product;



// return (
//     <Row className="mt-3 mb-3">
//             <Col xs={12}>
//                 <Card className="cardHighlight p-0">
//                     <Card.Body>
//                         <Card.Title><h4>{name}</h4></Card.Title>
//                         <Card.Subtitle>Description</Card.Subtitle>
//                         <Card.Text>{description}</Card.Text>
//                         <Card.Subtitle>Price</Card.Subtitle>
//                         <Card.Text>{price}</Card.Text>
//                         {/*<Card.Subtitle>Count: {count} </Card.Subtitle>
//                         <Card.Subtitle>Seats: {seats}</Card.Subtitle>*/}
//                         {/*<Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>*/}
//                         <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
//                     </Card.Body>`
//                 </Card>
//             </Col>
//     </Row>        
//     )
// }
