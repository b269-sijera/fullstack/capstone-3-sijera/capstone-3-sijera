import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';

import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import Swal from 'sweetalert2';

import {useParams, useNavigate, Link} from 'react-router-dom';

export default function ProductView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();
	// "useParams" hook that will allow us to retrieve the courseId passed via URL params
	// http://localhost:3000/courses/642b9ee764ba1150b3a1a4e1
	const {productId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

const order = (productId) => {
	fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
		method: "POST",
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			productId:productId
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		if(data === true) {
			Swal.fire({
				title: "THANK YOU FOR PURCHASING!",
				icon: "success",
				text: "You have successfully enrolled for this course."
			})

			navigate("/products")

		} else {
			Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})
		}

	})
};

useEffect(() => {
	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
}, [productId])

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card className="productCard mt-5">
					      <Card.Body className="text-center mt-3">
					        <Card.Title className="product-name">{name}</Card.Title>
					        <Card.Subtitle className="description">Description:</Card.Subtitle>
					        <Card.Text className="product-description">{description}</Card.Text>
					        <Card.Subtitle className="price">Price:</Card.Subtitle>
					        <Card.Text className="product-price">PHP {price}</Card.Text>
					        
					        {
					        	(user.id !== null) ?
					        		<Button className="bttn1 w-50" onClick={() => order(productId)} >Order</Button>
					        		:
					        		<Button className="bttn2 w-50" as={Link} to="/login"  >Log in to Order</Button>
					        }
					        <Button className=" bttn3 w-50 mx-3 mt-2" variant="danger" as={Link} to={`/products`}>Cancel</Button>

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}