import Button from 'react-bootstrap/Button';
import {useState, useEffect, useContext} from 'react';
import UserContext from "../UserContext";
import Swal from 'sweetalert2';import Card from 'react-bootstrap/Card';
import {useNavigate} from 'react-router-dom';
import {Link, NavLink} from 'react-router-dom';

 export default function Create() {
   return (
     <Card className="admin-card text-center mt-5 pb-4">
       <Card.Body>
         <Card.Title className="admin-title">Admin Page</Card.Title>
         <Button variant="info" className="my-3 mx-3 w-50 text-white" as={NavLink} to="/create">Create Product</Button>
         {/*<Button><Link className="text-decoration-none text-light" as={NavLink} to="/create">Show User Orders</Link></Button>*/}
         <Button className="w-50" as={Link} to={`/all`}>Retrieve/Update All Products</Button>
       </Card.Body>
     </Card>
   );
 }



