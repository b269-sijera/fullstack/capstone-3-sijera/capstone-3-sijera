import {useState, useEffect} from "react";

// import coursesData from '../data/coursesData';
import AllProductCard from '../components/AllProductCard';

export default function AllProduct() {

  // to store the courses retrieved from the database
  const [products, setProducts] = useState([]);

  /*console.log(coursesData);

  const courses = coursesData.map(course => {
    return (
      < CourseCard key={course.id} course = {course} />
    )
  })*/

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setProducts(data.map(product => {
          return(
            <AllProductCard key={product._id} product={product} />
          )
        }))
      })
    }, [])

  return (
    <>
    {products}
    </>
  )
}