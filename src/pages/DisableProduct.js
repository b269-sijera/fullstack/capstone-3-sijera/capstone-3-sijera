import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';
import { useParams, Link, NavLink } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function ArchiveProduct() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [isActive, setIsActive] = useState(false);
  const { productId } = useParams();

  useEffect(() => {
    setIsActive(false);
  }, [productId]);

  function disableProduct(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        isActive: !isActive, // toggle isActive between true and false
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          setIsActive(!isActive); // toggle isActive between true and false

          Swal.fire({
            title: 'Product status has been changed!',
            icon: 'success',
          });

          navigate('/products');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please, try again.',
          });
        }
      });
  }

  function toggleActive() {
    setIsActive(!isActive);
  }

  return (
    <Card className="text-center mt-5">
      <Card.Header>Change Product Status</Card.Header>
      <Card.Body>
        <Card.Title>
          Are you sure to {isActive ? 'disable' : 'make available'} this product?
        </Card.Title>
        <Button variant="primary" onClick={disableProduct}>
          {isActive ? 'Disable' : 'Available'}
        </Button>
        <Button className="mx-2" variant="secondary" onClick={toggleActive}>
          No
        </Button>
        <Button variant="danger" type="submit" id="submitBtn" as={Link} to={`/all`}>Cancel</Button>
      </Card.Body>
    </Card>
  );
}
