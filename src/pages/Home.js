import Banner from "../components/Banner";
import Highlights from "../components/Highlights"; 


export default function Home() {

	// const data = {
	// 	title: "BARE SKIN",
	// 	content: "Introducing our eco-friendly skincare brand, committed to providing effective and sustainable solutions for your skin. Our products are made with natural ingredients and packaged in biodegradable or recyclable materials, reducing our impact on the environment. We believe in ethical practices, transparency, and empowering you to make eco-conscious choices for your beauty routine..",
	// 	destination: "/products",
	// 	label: "Buy now!"
	// }

	return (
		<>
		<Banner/> 
		< Highlights/>
		</>
	)
}

