import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Navigate} from 'react-router-dom';
import UserContext from "../UserContext";
import Swal from 'sweetalert2';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Login(){

    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)

    function authenticate(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined") {
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: "Welcome to BARE SKIN!"
                }) 
            } else {
            Swal.fire({
                title: "Login Successful!",
                icon: "success",
                text: "Welcome to Zuitt!" 
            })
        }
               
    });


        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    // Check if the user is an admin and redirect to the admin page if true
    useEffect(() => {
        if (user.isAdmin) {
            <Navigate to="/admin"/>
        }
    }, [user])

    return(
        (user.id !== null) ?
        <Navigate to="/products"/>
        :
        <div className="wrapper d-flex align-items-center justify-content-center w-100">
            <div className="login rounded">
            <h2 className="login-title mb-3">Login Form</h2>
           
           <Form onSubmit={e => authenticate(e)}>
               <Form.Group controlId="userEmail">
                   <Form.Label id="name-label" >Email Address</Form.Label>
                   <Form.Control 
                       type="email" 
                       placeholder="Enter email"
                       value={email}
                       onChange={e => setEmail(e.target.value)}
                       required
                   />
               </Form.Group>

               <Form.Group className="mt-3" controlId="password">
                   <Form.Label id="name-label" >Password</Form.Label>
                   <Form.Control 
                       type="password" 
                       placeholder="Password"
                       value={password}
                       onChange={e => setPassword(e.target.value)}
                       required
                   />
               </Form.Group>

               {   isActive ?
                   <Button className=" bttn mt-4" variant="success" type="submit" id="submitBtn">
                       Log In
                   </Button>
                   :
                   <Button className=" bttn mt-4" variant="secondary" type="submit" id="submitBtn" disabled>
                       Log In
                   </Button>
               }
           </Form>
            </div> 
        </div>
        )
}
