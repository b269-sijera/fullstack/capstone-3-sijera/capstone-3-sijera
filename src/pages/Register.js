import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import { Form, Button } from 'react-bootstrap';

export default function Register() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    // to store values of the input fields
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

// useEffect(() => {
//     if((email !== "" && password1 !== "" && password2) && (password1 === password2)) {
//         setIsActive(true);
//     } else {
//         setIsActive(false);

//     }
// }, [email, password1, password2]);

    useEffect(() => {
    if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {

        setIsActive(true)
    } else {
        setIsActive(false)
    }
}, [firstName, lastName, email, mobileNo, password1, password2])

    // function to simulate user registration
    function registerUser(e) {
        // Prevents page from reloading       
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {

                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Kindly provide another email to complete registration."
                })
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true) {

                        // Clear input fields
                        setFirstName("");
                        setLastName("")
                        setEmail("");
                        setMobileNo("");
                        setPassword1("");
                        setPassword2("");

                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to BARE SKIN!"
                        })

                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }

                })
            }
        })

        // Clear input fields
        // setEmail("");
        // setPassword1("");
        // setPassword2("");

        // alert('Thank you for registering!');
    };

    return (
        (user.id !== null)?
        <Navigate to ="/products" />
        :
        <div className="wrapper d-flex align-items-center justify-content-center w-100 my-5">
                   <div className="register rounded">
                   <h2 className="login-title mb-3">Register Form</h2>
                  <Form onSubmit={(e) => registerUser(e)} >

                    <Form.Group className="mb-3" controlId="firstName">
                      <Form.Label id="name-label">First Name</Form.Label>
                      <Form.Control 
                          type="text"
                          value={firstName}
                          onChange={(e) => {setFirstName(e.target.value)}}
                          placeholder="Enter your First Name" 
                          required
                          />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="lastName">
                      <Form.Label id="name-label">Last Name</Form.Label>
                      <Form.Control 
                          type="text"
                          value={lastName}
                          onChange={(e) => {setLastName(e.target.value)}}
                          placeholder="Enter your Last Name" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="userEmail">
                      <Form.Label id="name-label">Email address</Form.Label>
                      <Form.Control 
                          type="email"
                          value={email}
                          onChange={(e) => {setEmail(e.target.value)}}
                          placeholder="Enter email" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="mobileNo">
                      <Form.Label id="name-label">Mobile Number</Form.Label>
                      <Form.Control 
                          type="Number"
                          value={mobileNo}
                          onChange={(e) => {setMobileNo(e.target.value)}}
                          placeholder="Enter Mobile Number" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password1">
                      <Form.Label id="name-label">Password</Form.Label>
                      <Form.Control 
                          type="password" 
                          value={password1}
                          onChange={(e) => {setPassword1(e.target.value)}}
                          placeholder="Enter Your Password" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password2">
                      <Form.Label id="name-label">Verify Password</Form.Label>
                      <Form.Control 
                          type="password" 
                          value={password2}
                          onChange={(e) => {setPassword2(e.target.value)}}
                          placeholder="Verify Your Password" />
                    </Form.Group>
                    { isActive ?
                              <Button className="bttn w-50" variant="success" type="submit" id="submitBtn">
                               Sign Up
                              </Button>
                              :
                              <Button className="bttn w-50" variant="secondary" type="submit" id="submitBtn" disabled>
                                Sign Up
                              </Button>
                    }
                 
                  </Form>
                   </div> 
               </div>

       
         
        
    )

}

 