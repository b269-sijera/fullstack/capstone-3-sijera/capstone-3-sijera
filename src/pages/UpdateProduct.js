import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';
import { useParams, Link, NavLink} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function ChangeProduct() {
  const {user} = useContext(UserContext);
  const navigate = useNavigate();
  const [active, setActive] = useState(false);
  const {productId} = useParams();

   const [name, setName] = useState('');
   const [description, setDescription] = useState('');
   const [price, setPrice] = useState('');

       useEffect(() => {
       if((name !== "" && price !== "" && description !== "")) 
       {
           setActive(true)
       } else {
           setActive(false)
       }
   }, [name, price, description])

       function updateProduct(e) {
         // Prevents page from reloading
         e.preventDefault();

         fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
           method: "PUT",
           headers: {
             'Content-Type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem('token')}`
           },
           body: JSON.stringify({
             productId: productId,
             name: name,
             description: description,
             price: price
           })
         })
         .then(res => res.json())
         .then(data => {
           console.log(data)

           if (data === true) {
             setName("");
             setPrice("");
             setDescription("");

             Swal.fire({
               title: "Update Product Successfully",
               icon: "success",
               text: "New Product!"
             });

             navigate("/products");
           } else {
             Swal.fire({
               title: "Something went wrong",
               icon: "error",
               text: "Please, try again."
             });
           }
         })
       }



   return (
     <div className="wrapper d-flex align-items-center justify-content-center w-100">
            <div className="update rounded">
            <h2 className="update-title mb-3">Update Product</h2>
           <Form onSubmit={(e) => updateProduct(e)} >
             <Form.Group className="mb-3" controlId="formGroupEmail">
               <Form.Label id="name-label">Product Name:</Form.Label>
               <Form.Control type="text" value={name} onChange={(e) => {setName(e.target.value)}} placeholder="Enter Product Name" />
             </Form.Group>
             <Form.Group className="mb-3" controlId="mobileNo">
               <Form.Label id="name-label">Price</Form.Label>
               <Form.Control 
                   type="Number"
                   value={price}
                   onChange={(e) => {setPrice(e.target.value)}}
                   placeholder="Enter Price" />
             </Form.Group>
             <Form.Group className="mb-3" controlId="formGroupPassword">
               <Form.Label id="name-label">Description:</Form.Label>
               <Form.Control as="textarea" rows={3} value={description} onChange={(e) => {setDescription(e.target.value)}} placeholder="Enter Description" />
             </Form.Group>
             { active ?
                       <Button variant="primary" type="submit" id="submitBtnUpdate">
                        Update
                       </Button>
                       :
                       <Button variant="primary" type="submit" id="submitBtnUpdate" disabled>
                         Update
                       </Button>
             }
             <Button variant="danger" id="submitBtnUpdate"><Link className="text-decoration-none text-light" as={NavLink} to="/all">Cancel</Link>
             </Button>
           </Form>
            </div> 
        </div>

   
   );
 };


 